# README of FrontISTR on HEC-MW

2020.11.13  Version 5.1.1

FrontISTR Commons

Comments, Questions, Problems etc.

e-mail : support@frontistr.com

### Programs in this archive
FrontISTR : Open-Source Large-Scale Parallel FEM Program for Nonlinear Structural Analysis

### Detailed manuals

  - [English](https://frontistr-commons.gitlab.io/FrontISTR_manual/en/)
  - [Japanese](https://frontistr-commons.gitlab.io/FrontISTR_manual/ja/)

### Files in this directory
README.md                   : README (in English : this file)  
README.ja.md                : README (in Japanese)  
VERSION                     : version information  
setup.sh & setup_fistr.sh   : shell script to create makefiles  
Makefile.am                 : base file of makefile for   installation  
Makefile.conf               : setting file for users  
Makefile.dev                : setting file for developers  

doc/                        : documents  
tutorial/                   : tutorial data  
examples/                   : some examples  
fisrt1/                     : FrontISTR  
hecmw1/                     : HEC-MW  
etc/                        : various setting files

#### NOTICE
Please read "License.txt" carefully BEFORE you start to use this software.
